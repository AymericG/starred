starred package
===============

Subpackages
-----------

.. toctree::

    starred.deconvolution
    starred.psf
    starred.optim
    starred.procedure
    starred.plots
    starred.utils


Module contents
---------------

.. automodule:: starred
    :members:
    :undoc-members:
    :show-inheritance: