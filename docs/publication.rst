Related published work 
======================

You can find the articles using STARRED below. Please let us know if you have published a paper that makes use of it for us to include it in this list.

* Galan et al. (2024). El Gordo needs El Anzuelo: Probing the structure of cluster members with multi-band extended arcs in JWST data. `https://arxiv.org/abs/2402.18636 <https://arxiv.org/abs/2402.18636>`_
* North et al. (2024). Polarimetry of the Ly-alpha envelope of the radio-quiet quasar SDSS J124020.91+145535.6. A&A, `https://doi.org/10.1051/0004-6361/202347423 <https://doi.org/10.1051/0004-6361/202347423>`_
* Dux et al. (2024). Nine lensed quasars and quasar pairs discovered through spatially-extended variability in Pan-STARRS. A&A 682, A47, `https://doi.org/10.1051/0004-6361/202347598 <https://doi.org/10.1051/0004-6361/202347598>`_
* Dux et al. (2023). PSJ2107-1611: a new wide-separation, quadruply imaged lensed quasar with flux ratio anomalies. A&A 679, L4, `https://doi.org/10.1051/0004-6361/202348227 <https://doi.org/10.1051/0004-6361/202348227>`_
