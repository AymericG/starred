Installation
============


Requirements
------------

STARRED requires Python 3.9 but we recommand to use Python 3.11 to benefit from the latest JAX updates.

It requires ``astropy``, ``dill``, ``jax``, ``jaxlib``, ``jaxopt``, ``matplotlib``, ``numpy``, ``scipy``, ``optax``, ``tqdm``, ``pyregion`` and ``h5py`` to work properly.
``emcee`` and ``mclmc`` are optional. We recommend to regularly update your ``jaxopt``, ``jaxlib`` and ``jax`` libraries, which are under constant development. New releases regularly improve the performance of STARRED.

Through PyPI
------------

STARRED releases are distributed through the Python Package Index (PyPI). To install the latest version use ``pip``:

::

    $ pip install starred-astro

   
Through Anaconda 
----------------

We provide an Anaconda environment that satisfies all the dependencies in ``starred-env.yml``.

::

    $ git clone https://gitlab.com/cosmograil/starred.git
    $ cd starred
    $ conda env create -f starred-env.yml
    $ conda activate starred-env
    $ pip install .

In case you have an NVIDIA GPU, this should automatically download the right version of JAX as well as cuDNN.
Next, you can run the tests to make sure your installation is working correctly.

::

    # While still in the starred directory:
    $ pytest . 

    
Manually handling the dependencies
----------------------------------

If you want to use an existing environment, just omit the Anaconda commands above:
::

    $ git clone https://gitlab.com/cosmograil/starred
    $ cd starred 
    $ pip install .


or if you need to install it for your user only:

::

	$ python setup.py install --user


JAX GPU installation
--------------------

STARRED is much faster on GPU, so we recommend to install a version of JAX that is compatible
with your version of CUDA and cuDNN, for example:

::

    $ pip install "jax[cuda11_cudnn86]" -f https://storage.googleapis.com/jax-releases/jax_cuda_releases.html
