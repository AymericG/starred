Citing STARRED in a publication
===============================

If you want to acknowledge STARRED in a publication, we suggest you cite our papers as follows:

* For the code itself: Michalewicz et al. (2023). STARRED: a two-channel deconvolution method with Starlet regularization. Journal of Open Source Software, 8(85), 5340, `https://doi.org/10.21105/joss.05340 <https://doi.org/10.21105/joss.05340>`_
* For the method: Millon et al. (2024). Image deconvolution and PSF reconstruction with STARRED: a wavelet-based two-channel method optimized for light curve extraction. `https://arxiv.org/abs/2402.08725 <https://arxiv.org/abs/2402.08725>`_
