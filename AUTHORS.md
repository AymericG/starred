# Credits

## STARRED

Development Lead:

- Kevin Michalewicz [[Website](https://kevinmichalewicz.com/)|[GitLab](https://gitlab.com/kmichalewicz)|[Email](mailto:k.michalewicz22@imperial.ac.uk?subject=[STARRED]%20Insert%20subject)]
- Martin
  Millon [[Website](https://martin-millon.gitlab.io/)|[GitLab](https://gitlab.com/martin-millon)|[Email](mailto:martin.millon@stanford.edu?subject=[STARRED]%20Insert%20subject)]
- Frédéric Dux [[Website](https://people.epfl.ch/frederic.dux?lang=en)|[GitLab](https://gitlab.com/duxfrederic)|[Email](mailto:frederic.dux@epfl.ch?subject=[STARRED]%20Insert%20subject)]

COSMOGRAIL's Principal Investigator (PI):

- Frédéric Courbin [[Website](https://people.epfl.ch/frederic.courbin?lang=en)|[GitLab](https://gitlab.com/courbin)|[Email](mailto:frederic.courbin@epfl.ch?subject=[STARRED]%20Insert%20subject)]