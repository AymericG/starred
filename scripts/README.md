# The STARRED pipeline

This folder contains all the scripts needed to generate a (narrow) PSF and to deconvolve a single image or a set of observations.

## 1. PSF Generation

You should first define a working directory that must contain the following paths:
* *(Required)* `data` with observed stars as `.npy` and/or `.fits`.
* *(Required)* `output`, in which the results will be saved.
* *(Optional)* `noise_map` with the 1sigma noise map corresponding to each observation as `.npy` and/or `.fits`. You can
  also include in this folder `.reg` files to mask contaminated regions (e.g. a close companion star). Given the i-th
  observation (with i starting in 0), the associated region file must be called `mask_i.reg`.

If you don't provide noise maps, STARRED will compute it __assuming that your data are in electrons__.

Then, to generate a narrow PSF:

    python 1_generate_psf.py --subsampling-factor FACTOR --niter N_ITER --lambda-scales L_SCALES --lambda-hf L_HF [--noise_map] --data-path DATA_PATH [--noise-map-path NOISE_MAP_PATH] --output-path OUTPUT_PATH [--plot-interactive] --float64

You can try to run for example:

    python 1_generate_psf.py --data-path ./data_test/PSF/data --noise-map-path ./data_test/PSF/noise_maps --output-path ./data_test/PSF/output --plot-interactive

Make sure that the Moffat fit is converged to a reasonable solution. If this is not the case you can try another
optimiser by adding the option  `--optim-analytical Newton-CG`. By default, STARRED uses `l-bfgs-b` which is very
efficient but sometimes get stuck in local minima.
To display all the other options , type:

    python 1_generate_psf.py --help
## 2. Deconvolution

You should first define a working directory that must contain the following subfolders:
* *(Required)* `data` with the observation(s) as `.npy`.
* *(Required)* `psf` with the corresponding psf(s) as `.npy`.
* *(Required)* `output`, in which the results will be saved.
* *(Optional)* `noise_map` with the noise map(s) corresponding the observation(s) as `.npy`.

Then, to deconvolve an image:

    python 2_deconvolve.py --subsampling-factor FACTOR --niter N_ITER --lambda-scales L_SCALES --lambda-hf L_HF [--noise_map] --point-sources M --data-path DATA_PATH [--noise_map_path NOISE_MAP_PATH] --psf-path PSF_PATH --output-path OUTPUT_PATH

If `M` is not 0, you should also include the initial position of the point sources by adding the following option :

    --cx 0.5 -0.5 --cy -0.5 0.5

Coordinates are defined from the center of the image and counted in pixels.

To test that everything is running fine, you can try :

    python3 2_deconvolve.py --point-sources 4 --cx -3.22 -1.88 1.63 2.1 --cy 0.03 -1.23 -2.77  4.6 --a 500 500 500 500 --data-path ./data_test/Deconv/data/ --psf-path ./data_test/Deconv/narrow_psf/ --output-path ./data_test/Deconv/output --subsampling-factor 2 --plot-interactive --astrometric-prior 0.25 --lambda-scales 3 --lambda-hf 5

In this example, we use only 4 images, so the reconstructed image is still a bit noisy. To counteract this, you can try
to rise `lambda-hf` to 100 and see the difference. The correlated noise in the reconstructed background should be
reduced.

For all other options, type :

    python 2_deconvolve.py --help
    

  